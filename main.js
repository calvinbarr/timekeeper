/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. 
 */



chrome.app.runtime.onLaunched.addListener(function(){
	var screenWidth= screen.availWidth;
	var screenHeight = screen.availHeight;
	
	var width=300;
	var height=250;
	chrome.app.window.create('index.html', {
		id:'helloworldId',
		    bounds:{
		    width:width,
			height:height,
			left:Math.round((screenWidth-width)/2),
			top:Math.round((screenHeight-height)/2)
			}
	    });
    });