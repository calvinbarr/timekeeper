/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. 
 */


var TimeKeeper= function(){
    

};
TimeKeeper.prototype.cb=function(eventName, Args){
    var bindargs = [this];
    if (Object.prototype.toString.call(Args) !== '[object Array]'){
	Args = [];
    }
    var bind = Function.prototype.bind;
    return bind.apply(this[eventName], bindargs.concat(Args));
};

TimeKeeper.prototype.initialize = function(){
    this.data = {};
    $('#checkin').click(this.cb('checkin'));
    $('#checkout').click(this.cb('checkout'));
    $('#starttime').click(this.cb('starttime'));
    $('#reset').click(this.cb('reset'));
    setInterval(this.cb('timelapsed'), 1000 );

    $('#addbreak').click(this.cb('addbreak'));
};

TimeKeeper.prototype.timelapsed = function(){
    if ( this.data.state === 'checkout'){
	return;
    }
    $('#timelapsed').html('Time lapsed ' + this.elapsed());
};

TimeKeeper.prototype.elapsed = function(){
    var timenow = new Date().getTime();
    var lastcheckin = this.data.checkin;
    var timeDiff = this.data.total + ( timenow - lastcheckin);
    timeDiff /= 1000;
    var seconds = Math.round(timeDiff % 60);
    timeDiff = Math.floor(timeDiff / 60);
    var minutes = Math.round(timeDiff % 60);
    timeDiff = Math.floor(timeDiff / 60);
    var hours = Math.round(timeDiff % 24);
    return ("00" +hours).slice(-2) + ":"+ ("00"+ minutes).slice(-2) + ":" + ("00" + seconds).slice(-2);

};

TimeKeeper.prototype.checkin = function(){
    this.data.checkin = new Date().getTime();
    this.data.state = 'checkin';
};

TimeKeeper.prototype.checkout = function(){
    var D = new Date();
    var tmp = new Date();
    D.setTime(tmp.getTime() + ( 1000 * 60 * 6));
    this.data.checkout = D.getTime();
    this.data.total = this.data.total + (this.data.checkout - this.data.checkin);
    this.data.state = 'checkout';
};


TimeKeeper.prototype.addbreak= function(){
    this.data.total = this.data.total - ( 1000 * 60 * 5);
}

TimeKeeper.prototype.starttime = function(){
    console.log('startime clicked');
    if ( $('#starttimetxt').val() === ''){
	this.data.startAt = new Date().getTime();
	this.data.checkin = this.data.startAt;
    }else {
	var D = new Date();
	var K = $('#starttimetxt').val().split(',');
	D.setHours(K[0]);
	D.setMinutes(K[1]);
	this.data.startAt=D.getTime();
	this.data.checkin = this.data.startAt;
	
    }
    this.data.total = 0;
    this.data.state="checkin";
    
};


TimeKeeper.prototype.reset = function(){
    this.data ={};
    console.log('checkout clicked');
};

var b = new TimeKeeper();
b.initialize();

